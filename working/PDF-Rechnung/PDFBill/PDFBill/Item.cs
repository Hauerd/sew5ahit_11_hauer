﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PDFBill
{
    class Item
    {
        public int Id { get; set; }
        public double Amount { get; set; }
        public double Discount { get; set; }
        public int ItemNumber { get; set; }
        public string Description { get; set; }
        public double Price { get; set; }
        public string Unit { get; set; }
        public double Tax { get; set; }

        public Item(int id, double amount, double discount, int itemNumber, string description, double price, string unit, double tax)
        {
            Id = id;
            Amount = amount;
            Discount = discount;
            ItemNumber = itemNumber;
            Description = description;
            Price = price;
            Unit = unit;
            Tax = tax;
        }
    }
}
