﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SQLite;

namespace PDFBill
{
    class DBConnection
    {
        private static DBConnection conn = null;

        public static DBConnection GetInstance()
        {
            if (conn == null)
            {
                conn = new DBConnection();
                conn.sqliteconnection.Open();
            }

            return conn;
        }





        SQLiteConnection sqliteconnection;

        private DBConnection()
        {
            sqliteconnection = new SQLiteConnection("Data Source=orders.sqlite;Version=3;");
        }

        public List<Order> ListOrders()
        {
            double tolerance = 0.01;


            List<Order> orders = new List<Order>();
            List<Item> items = new List<Item>();
            List<Customer> customers = new List<Customer>();

            string sql = "select o.*, c.*, i.*, oi.amount, oi.discount from `order` o, item i, ordertoitem oi, customer c where oi.iid = i.iid and oi.oid = o.oid and c.cid = o.cid order by i.iid;";
            
            SQLiteCommand cmd = new SQLiteCommand(sql, sqliteconnection);
            SQLiteDataReader reader = cmd.ExecuteReader();

            while (reader.Read())
            {
                int cid = (int)(long)reader["cid"];
                int oid = (int)(long)reader["oid"];

                // amount and discount needed to uniquely identify an item
                double amount = (double)(decimal)reader["amount"];
                double discount = (double)(decimal)reader["discount"];
                int iid = (int)(long)reader["iid"];

                Order order = orders.Find((o) => o.Id == oid);
                Item item = items.Find((i) => i.Id == iid && Math.Abs(i.Discount - discount) < tolerance && Math.Abs(i.Amount - amount) < tolerance);
                Customer customer = customers.Find((c) => c.Id == cid);

                if (order == null)
                {
                    string billnumber = (string) reader["billnumber"];
                    DateTime billdate = DateTime.Parse(reader["billdate"] as string);
                    DateTime ordertime = DateTime.Parse(reader["orderdate"] as string);
                    DateTime deliveryDate = DateTime.Parse(reader["deliverydate"] as string);

                    order = new Order(oid, new List<Item>(), billnumber, billdate, ordertime, deliveryDate, null);
                    orders.Add(order);
                }

                if (item == null)
                {
                    int itemNumber = (int) (long) reader["itemnumber"];
                    string desc = (string) reader["description"];
                    double price = (double) (decimal) reader["price"];
                    string unit = (string) reader["unit"];
                    double tax = (double) (decimal) reader["tax"];

                    item = new Item(iid, amount, discount, itemNumber, desc, price, unit, tax);
                    items.Add(item);
                }

                if (customer == null)
                {
                    int number = (int) (long) reader["customernumber"];
                    string name = (string) reader["name"];
                    string place = (string) reader["place"];
                    string street = (string) reader["street"];
                    string zip = (string) reader["zip"];

                    customer = new Customer(cid, number, name, place, street, zip);
                    customers.Add(customer);

                    order.Customer = customer;
                }

                order.Items.Add(item);
            }

            return orders;
        }
    }
}
