﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PDFBill
{
    class Order
    {
        public int Id;
        public List<Item> Items { get; set; }
        public string BillNumber { get; set; }
        public DateTime BillDate { get; set; }
        public DateTime OrderTime { get; set; }
        public DateTime DeliveryDate { get; set; }
        public Customer Customer { get; set; }

        public Order(int id, List<Item> items, string billNumber, DateTime billDate, DateTime orderTime, DateTime deliveryDate, Customer c)
        {
            Id = id;
            Items = items;
            BillNumber = billNumber;
            BillDate = billDate;
            OrderTime = orderTime;
            DeliveryDate = deliveryDate;
            Customer = c;
        }

        public override string ToString()
        {
            return BillNumber + ")\t" + "".PadLeft(5) + OrderTime.ToString("d") + "".PadLeft(5) + Customer.Name;
        }
    }
}
