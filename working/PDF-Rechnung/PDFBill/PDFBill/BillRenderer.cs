﻿using System.Diagnostics;
using MigraDoc.DocumentObjectModel;
using MigraDoc.Rendering;
using PdfSharp.Pdf;
using MigraDoc.DocumentObjectModel.Tables;
using System.IO;
using MigraDoc.DocumentObjectModel.Shapes;



namespace PDFBill
{
    class BillRenderer
    {
        static Document document;
        static Table table;
        static TextFrame addressFrame;
        static TextFrame addressFrame2;
        static Table table2;

        public static void RenderBillToPdf(Order order, string file)
        {
            document = CreateDocument(order);

            document.UseCmykColor = true;
            const bool unicode = false;

            const PdfFontEmbedding embedding = PdfFontEmbedding.Always;
            PdfDocumentRenderer pdfRenderer = new PdfDocumentRenderer(unicode, embedding);
            // Associate the MigraDoc document with a renderer
            pdfRenderer.Document = document;
            // Layout and render document to PDF
            pdfRenderer.RenderDocument();
            // Save the document...
            string filename = file;
            if (File.Exists(filename))
                File.Delete(filename);
            pdfRenderer.PdfDocument.Save(filename);
            // ...and start a viewer.
            Process.Start(filename);
        }

        private static Document CreateDocument(Order order)
        {
            // Create a new MigraDoc document
            document = new Document();
            document.Info.Title = "Rechnung";
            document.Info.Subject = "SEW Rechnung";
            document.Info.Author = "Dominik Hauer & Sebastian Kaupper";

            DefineStyles();

            CreatePage();

            FillContent(order);

            return document;
        }

        private static void DefineStyles()
        {
            // Get the predefined style Normal.
            Style style = document.Styles["Normal"];
            // Because all styles are derived from Normal, the next line changes the 
            // font of the whole document. Or, more exactly, it changes the font of
            // all styles and paragraphs that do not redefine the font.
            style.Font.Name = "Verdana";

            style = document.Styles[StyleNames.Header];
            style.ParagraphFormat.AddTabStop(new Unit(16, UnitType.Centimeter), MigraDoc.DocumentObjectModel.TabAlignment.Right);

            style = document.Styles[StyleNames.Footer];
            style.ParagraphFormat.AddTabStop(new Unit(8, UnitType.Centimeter), MigraDoc.DocumentObjectModel.TabAlignment.Center);

            // Create a new style called Table based on style Normal
            style = document.Styles.AddStyle("Table", "Normal");
            style.Font.Name = "Verdana";
            style.Font.Name = "Times New Roman";
            style.Font.Size = 9;

            // Create a new style called Reference based on style Normal
            style = document.Styles.AddStyle("Reference", "Normal");
            style.ParagraphFormat.SpaceBefore = "5mm";
            style.ParagraphFormat.SpaceAfter = "5mm";
            style.ParagraphFormat.TabStops.AddTabStop(new Unit(16, UnitType.Centimeter), MigraDoc.DocumentObjectModel.TabAlignment.Right);
        }

        private static void CreatePage()
        {
            // Each MigraDoc document needs at least one section.
            Section section = document.AddSection();
            section.PageSetup.DifferentFirstPageHeaderFooter = true;
            // Put a logo in the header
            Image image = section.Headers.FirstPage.AddImage("Mario.jpg");
            image.Height = "2.5cm";
            image.LockAspectRatio = true;
            image.RelativeVertical = RelativeVertical.Line;
            image.RelativeHorizontal = RelativeHorizontal.Margin;
            image.Top = ShapePosition.Top;
            image.Left = ShapePosition.Left;
            image.WrapFormat.Style = WrapStyle.Through;

            var p = section.Footers.Primary.AddParagraph();
            p.AddText("FN 98765w");
            p.AddTab();
            p.AddTab();
            p.AddTab();
            p.AddTab();
            p.AddTab();
            p.AddSpace(6);
            p.AddText("DVR: 0686568");
            p.AddLineBreak();
            p.AddText("Handelsgericht Wien");
            p.AddTab();
            p.AddTab();
            p.AddTab();
            p.AddTab();
            p.AddTab();
            p.AddSpace(6);
            p.AddText("ATU 87654321");

            p = section.Footers.FirstPage.AddParagraph();
            p.AddText("FN 98765w");
            p.AddTab();
            p.AddTab();
            p.AddTab();
            p.AddTab();
            p.AddTab();
            p.AddSpace(6);
            p.AddText("DVR: 0686568");
            p.AddLineBreak();
            p.AddText("Handelsgericht Wien");
            p.AddTab();
            p.AddTab();
            p.AddTab();
            p.AddTab();
            p.AddTab();
            p.AddSpace(6);
            p.AddText("ATU 87654321");

            // Create the text frame for the address
            addressFrame = section.AddTextFrame();
            addressFrame.Height = "3.0cm";
            addressFrame.Width = "6.0cm";
            addressFrame.Left = ShapePosition.Right;
            addressFrame.RelativeHorizontal = RelativeHorizontal.Margin;
            addressFrame.Top = "1.6cm";
            addressFrame.RelativeVertical = RelativeVertical.Page;

            addressFrame2 = section.AddTextFrame();
            addressFrame2.Height = "3.0cm";
            addressFrame2.Width = "6.0cm";
            addressFrame2.Left = ShapePosition.Left;
            addressFrame2.RelativeHorizontal = RelativeHorizontal.Margin;
            addressFrame2.Top = "4.0cm";
            addressFrame2.RelativeVertical = RelativeVertical.Line;


            table2 = section.AddTable();
            table2.Style = "Table";
            table2.Borders.Left.Width = 0.5;
            table2.Borders.Right.Width = 0.5;
            table2.Rows.LeftIndent = 0;
            table2.Format.Alignment = ParagraphAlignment.Right;
            table2.Borders = null;


            table2.AddColumn("14cm");
            Column column2 = table2.AddColumn("2cm");
            column2.Format.Alignment = ParagraphAlignment.Right;

            var column21 = table2.AddColumn("3,5cm");
            column21.Format.Alignment = ParagraphAlignment.Left;

            Row row2 = table2.AddRow();
            row2.HeadingFormat = true;
            row2.Format.Font.Bold = true;
            row2.Cells[0].AddParagraph("");
            row2.Cells[1].AddParagraph("");


            section.AddParagraph().AddLineBreak();
            // Create the item table
            table = section.AddTable();
            table.Style = "Table";
            table.Borders.Color = new Color(81, 125, 192);
            table.Borders.Width = 0.25;
            table.Borders.Left.Width = 0.5;
            table.Borders.Right.Width = 0.5;
            table.Rows.LeftIndent = 0;

            // Before you can add a row, you must define the columns
            Column column = table.AddColumn("2cm");
            column.Format.Alignment = ParagraphAlignment.Center;

            column = table.AddColumn("4cm");
            column.Format.Alignment = ParagraphAlignment.Right;

            column = table.AddColumn("1,5cm");
            column.Format.Alignment = ParagraphAlignment.Right;

            column = table.AddColumn("1,5cm");
            column.Format.Alignment = ParagraphAlignment.Right;

            column = table.AddColumn("1,5cm");
            column.Format.Alignment = ParagraphAlignment.Center;

            column = table.AddColumn("2cm");
            column.Format.Alignment = ParagraphAlignment.Right;

            column = table.AddColumn("1,5cm");
            column.Format.Alignment = ParagraphAlignment.Right;

            column = table.AddColumn("2cm");
            column.Format.Alignment = ParagraphAlignment.Right;

            // Create the header of the table
            Row row = table.AddRow();
            row.HeadingFormat = true;
            row.Format.Alignment = ParagraphAlignment.Center;
            row.Format.Font.Bold = true;
            row.Shading.Color = new Color(235, 240, 249);

            row = table.AddRow();
            row.HeadingFormat = true;
            row.Format.Alignment = ParagraphAlignment.Center;
            row.Format.Font.Bold = true;
            row.Shading.Color = new Color(235, 240, 249);
            row.Cells[0].AddParagraph("Artikel-Nr");
            row.Cells[0].Format.Alignment = ParagraphAlignment.Left;
            row.Cells[1].AddParagraph("Artikel");
            row.Cells[1].Format.Alignment = ParagraphAlignment.Left;
            row.Cells[2].AddParagraph("Menge");
            row.Cells[2].Format.Alignment = ParagraphAlignment.Left;
            row.Cells[3].AddParagraph("Einheit");
            row.Cells[3].Format.Alignment = ParagraphAlignment.Left;
            row.Cells[4].AddParagraph("Preis (€)");
            row.Cells[4].Format.Alignment = ParagraphAlignment.Left;
            row.Cells[5].AddParagraph("Rabatt (%)");
            row.Cells[5].Format.Alignment = ParagraphAlignment.Left;
            row.Cells[6].AddParagraph("USt (%)");
            row.Cells[6].Format.Alignment = ParagraphAlignment.Left;
            row.Cells[7].AddParagraph("Betrag (€)");
            row.Cells[7].Format.Alignment = ParagraphAlignment.Left;


            table.SetEdge(0, 0, 6, 2, Edge.Box, MigraDoc.DocumentObjectModel.BorderStyle.Single, 0.75, Color.Empty);
        }

        private static void FillContent(Order order)
        {
            
            // Fill address in address text frame

            Paragraph paragraph = addressFrame.AddParagraph();
            paragraph.AddFormattedText("Star Road 4",TextFormat.Bold);
            paragraph.AddLineBreak();
            paragraph.AddFormattedText("69 Bowsers Festung",TextFormat.Bold);
            paragraph.AddLineBreak();
            paragraph.AddFormattedText("Telefon: +43696 6969696",TextFormat.Bold);
            paragraph.AddLineBreak();
            paragraph.AddFormattedText("Telefax: +43696 6969696-69",TextFormat.Bold);
            paragraph.AddLineBreak();
            paragraph.AddLineBreak();
            paragraph.AddLineBreak();
            paragraph.AddLineBreak();
            paragraph.AddLineBreak();
            paragraph.AddLineBreak();
            paragraph.AddText("Bowsers Festung, ");
            paragraph.AddDateField("dd.MM.yyyy");


            Paragraph paragraph2 = addressFrame2.AddParagraph();
            paragraph2.AddFormattedText(order.Customer.Name.ToString(), TextFormat.Bold);
            paragraph2.AddLineBreak();
            paragraph2.AddFormattedText(order.Customer.Street.ToString(), TextFormat.Bold);
            paragraph2.AddLineBreak();
            paragraph2.AddFormattedText(order.Customer.Place.ToString() + " "+ order.Customer.Zip.ToString(), TextFormat.Bold);
            paragraph2.AddLineBreak();
            paragraph2.AddLineBreak();
            paragraph2.AddLineBreak();
            paragraph2.AddFormattedText("Rechnung: "+order.BillNumber.ToString(),TextFormat.Bold);

            Row r1 = table2.AddRow();
            r1.Cells[0].AddParagraph("Kd-Nr:");
            r1.Cells[1].AddParagraph(order.Customer.CustomerNumber.ToString());
            Row r2 = table2.AddRow();
            r2.Cells[0].AddParagraph("Ihre UID:");
            r2.Cells[1].AddParagraph(order.Id.ToString());
            Row r3 = table2.AddRow();
            r3.Cells[0].AddParagraph("");
            r3.Cells[1].AddParagraph("");
            Row r4 = table2.AddRow();
            r4.Cells[0].AddParagraph("Bestelldatum:");
            r4.Cells[1].AddParagraph(order.OrderTime.ToString("d"));
            Row r5 = table2.AddRow();
            r5.Cells[0].AddParagraph("Lieferdatum:");
            r5.Cells[1].AddParagraph(order.DeliveryDate.ToString("d"));

            // Iterate the invoice items
            double totalExtendedPrice = 0;
            foreach (Item item in order.Items)
            {
               Row row1 = table.AddRow();
                row1.Cells[0].AddParagraph(item.ItemNumber.ToString());
                row1.Cells[1].AddParagraph(item.Description.ToString());
                row1.Cells[2].AddParagraph(item.Amount.ToString());
                row1.Cells[3].AddParagraph(item.Price.ToString("0.00"));
                row1.Cells[4].AddParagraph(item.Unit.ToString());
                row1.Cells[5].AddParagraph(item.Discount.ToString());
                row1.Cells[6].AddParagraph(item.Tax.ToString());
                double extendedPrice = item.Amount * item.Price;
                extendedPrice = extendedPrice * (100 - item.Discount) / 100;
                row1.Cells[7].AddParagraph(extendedPrice.ToString("0.00"));
                totalExtendedPrice += extendedPrice;

                table.SetEdge(0, table.Rows.Count - 2, 6, 2, Edge.Box, MigraDoc.DocumentObjectModel.BorderStyle.Single, 0.75);
            }
           
            // Add an invisible row as a space line to the table
            Row row = table.AddRow();
            row.Borders.Visible = false;

            // Add the total price row
            row = table.AddRow();
            row.Cells[0].Borders.Visible = false;
            row.Cells[0].AddParagraph("Gesamtbetrag (inkl. Ust)");
            row.Cells[0].Format.Font.Bold = true;
            row.Cells[0].Format.Alignment = ParagraphAlignment.Right;
            row.Cells[0].MergeRight = 6;
            row.Cells[7].AddParagraph(totalExtendedPrice.ToString("0.00") + " €");

            // Set the borders of the specified cell range
            table.SetEdge(5, table.Rows.Count - 4, 1, 4, Edge.Box, MigraDoc.DocumentObjectModel.BorderStyle.Single, 0.75);

            // Add the notes paragraph
            paragraph = document.LastSection.AddParagraph();
            paragraph.Format.SpaceBefore = "1cm";
            paragraph.Format.Borders.Width = 0.75;
            paragraph.Format.Borders.Distance = 3;
            paragraph.Format.Borders.Color = new Color(81, 125, 192);
            paragraph.Format.Shading.Color = new Color(242, 242, 242);
            paragraph.AddText("Wir danken für den Autrag und ersuchen um Überweisung des Betrages auf unser Konto Nr. 123.456.789 bei der MUSTER BANK, BLZ 12345");
        }



    }
}
