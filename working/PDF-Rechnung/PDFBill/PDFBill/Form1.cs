﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using System.Data.SQLite;

namespace PDFBill
{
    public partial class Form1 : Form
    {
        private List<Order> orders = null; 
        
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            DBConnection db = DBConnection.GetInstance();
            orders = db.ListOrders();

            foreach (var o in orders)
            {
                comboBox1.Items.Add(o);
            }

            if (comboBox1.Items.Count > 0)
            {
                comboBox1.SelectedIndex = 0;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (comboBox1.SelectedIndex == -1)
            {
                MessageBox.Show("You must choose an order to print");
                return;
            }

            string filename = "";

            SaveFileDialog sd = new SaveFileDialog();
            sd.Filter = "PDF Document|*.pdf";
            sd.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);

            if (sd.ShowDialog() == DialogResult.OK)
            {
                filename = sd.FileName;
                Order o = comboBox1.SelectedItem as Order;
                BillRenderer.RenderBillToPdf(o, filename);
                MessageBox.Show("Operation successful");
            }
        }
    }
}
