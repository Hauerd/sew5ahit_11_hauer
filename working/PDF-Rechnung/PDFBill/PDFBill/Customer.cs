﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PDFBill
{
    class Customer
    {
        public int Id;
        public int CustomerNumber { get; set; }
        public string Name { get; set; }
        public string Place { get; set; }
        public string Street { get; set; }
        public string Zip { get; set; }

        public Customer(int id, int customerNumber, string name, string place, string street, string zip)
        {
            Id = id;
            CustomerNumber = customerNumber;
            Name = name;
            Place = place;
            Street = street;
            Zip = zip;
        }
    }
}
