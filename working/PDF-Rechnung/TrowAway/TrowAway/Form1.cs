﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Diagnostics;
using MigraDoc.DocumentObjectModel;
using MigraDoc.Rendering;
using MigraDoc.RtfRendering;
using PdfSharp.Pdf;
using MigraDoc.DocumentObjectModel.Tables;
using System.IO;

namespace TrowAway
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            Document document = CreateDocument();
            AddTable(document);

            document.UseCmykColor = true;
            const bool unicode = false;

            const PdfFontEmbedding embedding = PdfFontEmbedding.Always;
            PdfDocumentRenderer pdfRenderer = new PdfDocumentRenderer(unicode, embedding);
            // Associate the MigraDoc document with a renderer
            pdfRenderer.Document = document;
            // Layout and render document to PDF
            pdfRenderer.RenderDocument();
            // Save the document...
            const string filename = "HelloWorld3.pdf";
            if (File.Exists(filename))
                File.Delete(filename);
            pdfRenderer.PdfDocument.Save(filename);
            // ...and start a viewer.
            Process.Start(filename);
        }

        static Document CreateDocument()
        {

            Document document = new Document();
            document.AddSection();
           


            Paragraph paragraph = document.LastSection.AddParagraph();
            paragraph.Format.Font.Color = MigraDoc.DocumentObjectModel.Color.FromCmyk(100, 30, 20, 50);
            paragraph.AddFormattedText("Hello, World!", TextFormat.Bold);


            var img = document.LastSection.AddImage("totenkopf.png");
            img.RelativeVertical = MigraDoc.DocumentObjectModel.Shapes.RelativeVertical.Page;
            img.RelativeHorizontal = MigraDoc.DocumentObjectModel.Shapes.RelativeHorizontal.Page;
            img.WrapFormat.Style = MigraDoc.DocumentObjectModel.Shapes.WrapStyle.Through;
            img.WrapFormat.DistanceLeft = Unit.FromCentimeter(5);
            document.LastSection.AddPageBreak();
            return document;

        }

        void AddTable(Document doc)
        {
            Table table = new Table();
            table.Borders.Width = 1;

            table.AddColumn();
            table.AddColumn();
            Row row = table.AddRow();
            row.Shading.Color = MigraDoc.DocumentObjectModel.Color.FromCmyk(100, 30, 80, 50);

            Cell cell = row.Cells[0];
            cell.AddParagraph("Erste Zelle");
            cell = row.Cells[1];
            cell.AddParagraph("Zweite Zelle");

            Row row2 = table.AddRow();
            Cell cell2 = row2.Cells[0];
            cell2.AddParagraph("Erste Zelle");
            cell2 = row2.Cells[1];
            cell2.AddParagraph("Zweite Zelle");
            doc.LastSection.Add(table);

        }
    }
}

