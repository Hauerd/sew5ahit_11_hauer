﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMÜ
{
    public static class Extensions
    {
        public static string Fullname(this Customer c)
        {
            return "C#3.0: "+ c.Firstname + " " + c.Lastname;
        }
    }
}
