﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMÜ
{
    class Program
    {
        static void Main(string[] args)
        {
            //Extention Klasse
            Customer erster = new Customer();
            erster.Firstname = "Dominik";
            erster.Lastname = "Hauer";
            Console.WriteLine(erster.Fullname());

            //Vererbung mit Methode
            MyCustomer zweiter = new MyCustomer();
            zweiter.Firstname = "Sebastion";
            zweiter.Lastname = "Kaupper";
            Console.WriteLine(zweiter.Fullname());

            //Vererbung mit Propertie
            MyCustomer dritter = new MyCustomer();
            dritter.Firstname = "Christoph";
            dritter.Lastname = "Böhmwalder";
            Console.WriteLine(dritter.FullnameProb);

        }
    }
}
