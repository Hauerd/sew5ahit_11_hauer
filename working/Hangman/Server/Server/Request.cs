﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace Server
{
    class Request
    {
        string s = "";

        public Request(string s)
        {
            this.s = s;
        }

        public string RetContent()
        {
            XmlDocument xdoc = new XmlDocument();
            xdoc.LoadXml(s);

            string sx = xdoc.ChildNodes[0].InnerText;
            return sx;
        }
    }
}
