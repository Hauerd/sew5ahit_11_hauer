﻿#define LOG

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.IO;
using System.Net.Sockets;
using System.Xml;
using System.Net;

namespace Server
{
    class Program
    {
        static Dictionary<IPAddress, Hangman> dict = new Dictionary<IPAddress, Hangman>();

        static TcpListener listener;
        static int iport = 9998;

        static void Main(string[] args)
        {
            listener = new TcpListener(iport);
            listener.Start();
            

            Thread tstart = new Thread(new ThreadStart(mainThread));
            tstart.Start();

        }

        static internal void mainThread()
        {

            
            
            Hangman h = new Hangman();

                try
                {
                    while (true)
                    {
                        Socket soc = listener.AcceptSocket();
                        IPEndPoint remoteIpEndPoint = soc.RemoteEndPoint as IPEndPoint;
                        Stream s = new NetworkStream(soc);
                        StreamReader sr = new StreamReader(s);
                        StreamWriter sw = new StreamWriter(s);
                        sw.AutoFlush = true;

                        string sline = "";
                        
                        sline = sr.ReadLine();

                        Request r = new Request(sline);
                        sline = r.RetContent();

#if LOG
                        Console.WriteLine("Recived:" + sline);
#endif

                        if(sline == "0")
                        {
                            h = new Hangman();

                            if (dict.ContainsKey(remoteIpEndPoint.Address))
                                dict[remoteIpEndPoint.Address] = h;
                            else
                            dict.Add(remoteIpEndPoint.Address, h);

                            CreateResponse cr = new CreateResponse(h.guessWord, h.iguesses);
                            string sresponse = cr.makeXML();

                            sw.WriteLine(sresponse);
#if LOG
                            Console.WriteLine(sresponse);
#endif
                        }else
                            if(sline == "1")
                            {
                                if (dict.ContainsKey(remoteIpEndPoint.Address))
                                    dict.Remove(remoteIpEndPoint.Address);
                                //break;
                            }else
                            {
                                if (dict.ContainsKey(remoteIpEndPoint.Address))
                                {
                                    h = dict[remoteIpEndPoint.Address];
                                }
                                h.Guess(sline.ToCharArray()[0]);

                                CreateResponse cr = new CreateResponse(h.guessWord, h.iguesses);

                                string sresponse = cr.makeXML();

                                if (h.bend)
                                    sresponse = cr.makeXMLendedGame();

                                sw.WriteLine(sresponse);

#if LOG
                                Console.WriteLine(sresponse);
#endif
                            }

                        s.Close();
                        soc.Close();
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                }
            

                
         }
        


    }
}
