﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace Server
{
    class CreateResponse
    {
        string s;
        int ties;
        public CreateResponse(string s, int tries)
        {
            this.s = s;
            this.ties = tries;
            SplitString();
        }

        private void SplitString()
        {
            char[] carr = s.ToCharArray();
            string stmp = "";
            for (int i = 0; i < carr.Length; i++)
            {
                 stmp += carr[i]; 
                if(i != carr.Length-1)
                 stmp += " ";                   
            }

            s = stmp;

            if (ties >= 10)
            {
                s = "You Lost";
            }
            else
            if (!s.Contains('_'))
            {
                s = "You Won";
            }
        }

        public string makeXMLendedGame()
        {
            XmlDocument xdoc = new XmlDocument();
            XmlNode rootNode = xdoc.CreateElement("hangman");
            XmlNode word = xdoc.CreateElement("word");
            XmlNode tries = xdoc.CreateElement("tries");

            word.InnerText = "Game ended type in 0 to start new one.";
            tries.InnerText = ties.ToString();

            xdoc.AppendChild(rootNode);
            rootNode.AppendChild(word);
            rootNode.AppendChild(tries);
            return xdoc.InnerXml;
        }

        public string makeXML()
        {
            XmlDocument xdoc = new XmlDocument();
            XmlNode rootNode = xdoc.CreateElement("hangman");
            XmlNode word = xdoc.CreateElement("word");
            XmlNode tries = xdoc.CreateElement("tries");

            word.InnerText = s;
            tries.InnerText = ties.ToString();

            xdoc.AppendChild(rootNode);
            rootNode.AppendChild(word);
            rootNode.AppendChild(tries);
            return xdoc.InnerXml;
        }
    }
}
