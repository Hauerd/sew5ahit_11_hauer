﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Server
{
    class Hangman
    {
        private string sWord;
        private List<char> liLetters;
        internal int iguesses; // wrong guesses
        internal string guessWord = "";
        internal bool bend;
        internal Hangman()
        {
            bend = false;
            liLetters = new List<char>();
            GenerateWord();
        }

        public string Guess(char c)
        {
            if (iguesses >= 10)
                bend = true;
            if (!guessWord.Contains('_'))
                bend = true;

            Char[] carr = sWord.ToCharArray();

            if (carr.Contains(c))
            {
                StringBuilder sb = new StringBuilder(guessWord);
                for (int i = 0; i < carr.Length; i++)
                {
                    if(carr[i] == c)
                    sb[i] = c;
                }
                guessWord = sb.ToString();

            }else
            {
                if(!liLetters.Contains(c) && bend == false)
                iguesses++;                
            }

            liLetters.Add(c);
            return guessWord;
        }

        private void GenerateWord()
        {
            string[] sarr = {"hallo","hangman","test","enumeration","internetstick","stoppuhr" };

            Random r = new Random();
            sWord = sarr[r.Next(0, sarr.Count() - 1)];

            for (int i = 0; i < sWord.Length; i++)
            {
                guessWord += "_";
            }
        }
    }
}
