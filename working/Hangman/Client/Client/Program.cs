﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Sockets;
using System.Threading;
using System.IO;
using System.Xml;

namespace Client
{
    class Program
    {
        static Stream s;
        static TcpClient client;
        static void Main(string[] args)
        {
            // 0 Neues Spiel
            // 1 cancel
            // Buchstabe raten
            showOptions();
            Thread tsend = new Thread(write);
            tsend.Start();
        }
        static void write()
        {
            XmlDocument xdoc;

            while (true)
            {
                string sy = Console.ReadLine();
                
                client = new TcpClient();
                client.Connect("127.0.0.1", 9998);
                s = client.GetStream();
                StreamWriter sw = new StreamWriter(s);
                StreamReader sr = new StreamReader(s);
                sw.AutoFlush = true;

                xdoc = new XmlDocument();
                XmlNode rootNode = xdoc.CreateElement("hangman");
                rootNode.InnerText = sy;
                xdoc.AppendChild(rootNode);
                sw.WriteLine(xdoc.InnerXml);

                if (sy != "1")
                {
                    string sx = sr.ReadLine();

                    gernerateOutput(sx);
                }
                s.Close();
                client.Close();
            }
        }

        static void gernerateOutput(string sxml)
        {
            XmlDocument xdoc = new XmlDocument();
            xdoc.LoadXml(sxml);
            string sword = xdoc.ChildNodes[0].ChildNodes[0].InnerText;
            string stries = xdoc.ChildNodes[0].ChildNodes[1].InnerText;

            Console.WriteLine(sword+" Tries: "+stries);
        }

        static void showOptions()
        {
            Console.WriteLine("Type in 0 to start a new game");
            Console.WriteLine("Type in 1 to cancel a game");
            Console.WriteLine("Type in a letter to guess");
        }
    }
}
